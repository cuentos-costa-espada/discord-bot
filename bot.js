const { Client, Events, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');

const client = new Client({ 
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildMembers,
    ],
});

client.once(Events.ClientReady, c => {
	console.log(`Ready! Logged in as ${c.user.tag}`);
});

client.on("messageCreate", (message) => {
   
    const { spawn } = require('child_process');
    
    // Comandos que empiezan por !
	if (message.content.substring(0, 1) == '!') {
        var args = message.content.substring(1).split(' ');
        var command = args[0];      

        // Eliminamos mensaje
        message.delete()
        
        // Solo VinceMontana (por ahora) podrá usar los comandos
        if (message.author.id == 374274637649149955) {
            console.log(`[OK] Es Vince Montana (message.author.id: ${message.author.id})`); 
        } else {
            console.log(`[KO] No es Vince Montana (message.author.id: ${message.author.id})`); 
            return; 
        }

        // Comandos
        switch(command) {

            case 'estado':  
                console.log(`Comando !estado ejecutado.`);
				const bat = spawn('cmd.exe', ['/c', 'D:/Juegos/Servidor/NWN2/CCE_DiscordBot/CCE_Discord_Comando_Estado.bat']);          
            break;

            case 'reinicio':  
                console.log(`Comando !reinicio ejecutado.`);
				const bat2 = spawn('cmd.exe', ['/c', 'D:/Juegos/Servidor/NWN2/CCE_DiscordBot/CCE_Discord_Comando_Reinicio.bat']);
            break;
            
            case 'apagar':  
                console.log(`Comando !apagar ejecutado.`);
				const bat3 = spawn('cmd.exe', ['/c', 'D:/Juegos/Servidor/NWN2/CCE_DiscordBot/CCE_Discord_Comando_Apagar.bat']);              
            break;            
        }
    }
});

client.login(token);